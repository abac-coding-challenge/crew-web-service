package com.example.crew.utils.enums;

public enum UnsecuredRoutes {
    SIGN_IN_URL_POST("/api/tokens/**"),
    SIGN_UP_URL_POST("/api/users");

    private final String url;

    UnsecuredRoutes(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
