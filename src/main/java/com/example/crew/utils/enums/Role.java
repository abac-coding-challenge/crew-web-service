package com.example.crew.utils.enums;

public enum Role {
    ROLE_CAPTAIN("ROLE_CAPTAIN");

    private final String value;

    Role(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
