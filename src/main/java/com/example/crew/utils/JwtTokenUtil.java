package com.example.crew.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.crew.entities.User;
import com.example.crew.exceptions.AbstractException;
import com.example.crew.models.TokenStorage;
import com.example.crew.repositories.UserRepository;
import com.example.crew.utils.enums.ExceptionCodes;
import com.example.crew.utils.enums.Token;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

@Component
public class JwtTokenUtil implements Serializable {
    private static final long serialVersionUID = -2550185165626007488L;

    private static final long JWT_TOKEN_VALIDITY = 1000 * 60 * 60; // 1h in millis
    private final UserRepository userRepository;
    @Value("${jwt.publicKey}")
    private String publicKeyPath;
    @Value("${jwt.privateKey}")
    private String privateKeyPath;

    @Autowired
    public JwtTokenUtil(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String getIdFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);

        return claimsResolver.apply(claims);
    }

    public String generateToken(User user, String type) throws AbstractException {
        Map<String, Object> claims = new HashMap<>();

        claims.put("type", type);
        claims.put("email", user.getEmail());

        Token token = Token.valueOf(type);

        switch (token) {
            case ACCESS_TOKEN:
                return generateOneHourToken(claims, user.getId());
            case REFRESH_TOKEN:
                return generateOneDayToken(claims, user.getId());
            default:
                // this code shouldn't be reached
                throw new AbstractException(ExceptionCodes.INCORRECT_TOKEN_TYPE.getValue());
        }
    }

    private String generateOneHourToken(Map<String, Object> claims, String subject) {
        return Jwts.builder().setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY))
                .signWith(SignatureAlgorithm.RS512, generatePrivateKey())
                .compact();
    }

    private String generateOneDayToken(Map<String, Object> claims, String subject) {

        return Jwts.builder().setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 24))
                .signWith(SignatureAlgorithm.RS512, generatePrivateKey())
                .compact();
    }

    public Boolean validateToken(String token, String type) {
        final String id = getIdFromToken(token);

        Claims claims = getAllClaimsFromToken(token);
        String tokenType = (String) claims.get("type");

        try {
            Optional<User> user = userRepository.findById(id);

            if (user.isEmpty()) {
                return false;
            }

            if (type.equals(Token.ACCESS_TOKEN.name()) || type.equals(Token.REFRESH_TOKEN.name())) {
                String tokenFromStorage = TokenStorage.getInstance().getTokenById(id, type);
                if (tokenFromStorage == null || !tokenFromStorage.equals(token)) {
                    return false;
                }
            }
        } catch (Exception exception) {
            return false;
        }

        return (!isTokenExpired(token) && tokenType.equals(type) && validateSignKey(token));
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);

        return expiration.before(new Date());
    }

    private Claims getAllClaimsFromToken(String token) {

        return Jwts.parser().setSigningKey(generatePrivateKey()).parseClaimsJws(token).getBody();
    }

    private Boolean validateSignKey(String token) {
        final PublicKey publicKey = generatePublicKey();
        final PrivateKey privateKey = generatePrivateKey();

        final Algorithm algorithm = Algorithm.RSA512((RSAPublicKey) publicKey, (RSAPrivateKey) privateKey);

        final DecodedJWT decodedJWT = JWT.decode(token);

        try {
            algorithm.verify(decodedJWT);
        } catch (SignatureVerificationException exception) {
            return false;
        }

        return true;
    }

    private PrivateKey generatePrivateKey() {
        try {
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PemReader pemReader = new PemReader(new InputStreamReader(new FileInputStream(privateKeyPath)));
            PemObject pemObject = pemReader.readPemObject();

            byte[] content = pemObject.getContent();
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(content);

            return factory.generatePrivate(privateKeySpec);
        } catch (InvalidKeySpecException | IOException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }

    private PublicKey generatePublicKey() {
        try {
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PemReader pemReader = new PemReader(new InputStreamReader(new FileInputStream(publicKeyPath)));
            PemObject pemObject = pemReader.readPemObject();

            byte[] content = pemObject.getContent();
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(content);

            return factory.generatePublic(publicKeySpec);
        } catch (InvalidKeySpecException | IOException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }
}
