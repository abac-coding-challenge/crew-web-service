package com.example.crew;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrewWebServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrewWebServiceApplication.class, args);
    }
}
