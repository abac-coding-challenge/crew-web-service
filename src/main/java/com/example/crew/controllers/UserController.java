package com.example.crew.controllers;

import com.example.crew.dtos.UserDTO;
import com.example.crew.dtos.builders.UserBuilder;
import com.example.crew.exceptions.AbstractException;
import com.example.crew.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/users")
public class UserController {
    private final UserService userService;
    private final UserBuilder userBuilder;

    @Autowired
    public UserController(UserService userService, UserBuilder userBuilder) {
        this.userService = userService;
        this.userBuilder = userBuilder;
    }

    @PostMapping
    public ResponseEntity add(@RequestBody UserDTO userDTO) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(userBuilder.generateDTOFromEntity(userService.create(userDTO)));
        } catch (AbstractException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getErrorMessage());
        }
    }
}
