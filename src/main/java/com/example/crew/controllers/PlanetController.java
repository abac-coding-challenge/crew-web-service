package com.example.crew.controllers;

import com.example.crew.dtos.PlanetDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

import static com.example.crew.config.RestTemplateConfig.exchange;
import static com.example.crew.config.RestTemplateConfig.getHeaders;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/planets")
public class PlanetController {
    private static final String planetWebServiceUrl = "http://localhost:8090";
    private static final String planetUrl = planetWebServiceUrl + "/api/planets";
    public static final String HEADER_AUTHORIZATION_KEY = "Authorization";

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping
    public ResponseEntity findAll() throws URISyntaxException {
        final URI uri = new URI(planetUrl);
        final HttpEntity<PlanetDTO> entity = new HttpEntity<>(getHeaders());

        return exchange(restTemplate, uri, HttpMethod.GET, entity);
    }

    @PostMapping()
    public ResponseEntity create(@RequestBody PlanetDTO planetDTO)
            throws URISyntaxException {
        final URI uri = new URI(planetUrl);
        final HttpEntity<PlanetDTO> entity = new HttpEntity<>(planetDTO, getHeaders());

        return exchange(restTemplate, uri, HttpMethod.POST, entity);
    }

    @PatchMapping(value = "/{id}")
    public ResponseEntity update(@PathVariable String id,
                                 @RequestParam(name = "crewId", required = false) String crewId,
                                 @RequestBody PlanetDTO planetDTO) throws URISyntaxException {
        final String updateUrl = (crewId == null ? (planetUrl + "/" + id) : (planetUrl + "/" + id + "?crewId=" + crewId));
        final URI uri = new URI(updateUrl);
        final HttpEntity<PlanetDTO> entity = new HttpEntity<>(planetDTO, getHeaders());

        return exchange(restTemplate, uri, HttpMethod.PATCH, entity);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable String id)
            throws URISyntaxException {
        final String deleteUri = planetUrl + "/" + id;
        final URI uri = new URI(deleteUri);
        final HttpEntity<PlanetDTO> entity = new HttpEntity<>(getHeaders());

        return exchange(restTemplate, uri, HttpMethod.DELETE, entity);
    }
}
