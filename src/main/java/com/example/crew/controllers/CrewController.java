package com.example.crew.controllers;

import com.example.crew.dtos.builders.CrewBuilder;
import com.example.crew.services.CrewService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/crews")
public class CrewController {
    private final CrewService crewService;
    private final CrewBuilder crewBuilder;

    public CrewController(CrewService crewService, CrewBuilder crewBuilder) {
        this.crewService = crewService;
        this.crewBuilder = crewBuilder;
    }

    @GetMapping
    public ResponseEntity findAll() {
        return ResponseEntity.ok(crewBuilder.generateDTOsFromEntities(crewService.getAll()));
    }
}
