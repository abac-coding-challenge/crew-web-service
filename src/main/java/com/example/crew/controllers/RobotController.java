package com.example.crew.controllers;

import com.example.crew.dtos.RobotDTO;
import com.example.crew.dtos.builders.RobotBuilder;
import com.example.crew.exceptions.AbstractException;
import com.example.crew.services.RobotService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/robots")
public class RobotController {
    private final RobotService robotService;
    private final RobotBuilder robotBuilder;

    public RobotController(RobotService robotService, RobotBuilder robotBuilder) {
        this.robotService = robotService;
        this.robotBuilder = robotBuilder;
    }

    @GetMapping(value = "/{userId}")
    public ResponseEntity findAllByUserId(@PathVariable String userId) {
        return ResponseEntity.ok(robotBuilder.generateDTOsFromEntities(robotService.getAllByUserId(userId)));
    }

    @PostMapping(value = "/{userId}")
    public ResponseEntity create(@PathVariable String userId, @RequestBody RobotDTO robotDTO) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(robotBuilder.generateDTOFromEntity(robotService.add(userId, robotDTO)));
        } catch (AbstractException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getErrorMessage());
        }
    }

    @PatchMapping(value = "/{id}")
    public ResponseEntity update(@PathVariable String id, @RequestBody RobotDTO robotDTO) {
        try {
            return ResponseEntity.ok(robotBuilder.generateDTOFromEntity(robotService.edit(id, robotDTO)));
        } catch (AbstractException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getErrorMessage());
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable String id) {
        try {
            robotService.remove(id);

            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        } catch (AbstractException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getErrorMessage());
        }
    }
}
