package com.example.crew.controllers;

import com.example.crew.dtos.AuthorizationTokenDTO;
import com.example.crew.dtos.UserDTO;
import com.example.crew.exceptions.AbstractException;
import com.example.crew.services.AuthorizationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/tokens")
public class AuthorizationTokenController {
    private final AuthorizationTokenService authorizationTokenService;

    @Autowired
    public AuthorizationTokenController(AuthorizationTokenService authorizationTokenService) {
        this.authorizationTokenService = authorizationTokenService;
    }

    @PostMapping
    public ResponseEntity create(@RequestBody UserDTO userDTO) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(authorizationTokenService.createToken(userDTO));
        } catch (AbstractException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getErrorMessage());
        }
    }

    @PostMapping(path = "/refresh")
    public ResponseEntity refresh(@RequestBody AuthorizationTokenDTO authorizationTokenDTO) {
        try {
            return ResponseEntity.ok(authorizationTokenService.refreshToken(authorizationTokenDTO.getRefreshToken()));
        } catch (AbstractException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getErrorMessage());
        }
    }

    @DeleteMapping(value = "/{userId}")
    public ResponseEntity delete(@PathVariable String userId) {
        try {
            authorizationTokenService.removeToken(userId);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        } catch (AbstractException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getErrorMessage());
        }
    }
}
