package com.example.crew.exceptions;

import org.springframework.http.HttpStatus;

public class NotFoundException extends AbstractException {
    private final HttpStatus statusCode = HttpStatus.NOT_FOUND;

    public NotFoundException(String message) {
        super(message);
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }
}
