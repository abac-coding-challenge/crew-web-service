package com.example.crew.exceptions;

import com.example.crew.models.ErrorResponse;
import org.springframework.http.HttpStatus;

public class AbstractException extends Exception {
    private final HttpStatus statusCode = HttpStatus.BAD_REQUEST;

    public AbstractException(String message) {
        super(message);
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public ErrorResponse getErrorMessage() {
        return new ErrorResponse(this.getMessage());
    }
}
