package com.example.crew.models;


import com.example.crew.exceptions.AbstractException;
import com.example.crew.utils.enums.ExceptionCodes;
import com.example.crew.utils.enums.Token;

import java.util.HashMap;

public class TokenStorage {
    private static TokenStorage tokenStorage = null;

    private final HashMap<String, String> accessTokenStore;
    private final HashMap<String, String> refreshTokenStore;

    private TokenStorage() {
        accessTokenStore = new HashMap<>();
        refreshTokenStore = new HashMap<>();
    }

    public static synchronized TokenStorage getInstance() {
        if (tokenStorage == null)
            tokenStorage = new TokenStorage();

        return tokenStorage;
    }

    public void addToken(String id, String token, String type) throws AbstractException {
        try {
            Token tokenType = Token.valueOf(type);

            switch (tokenType) {
                case ACCESS_TOKEN:
                    accessTokenStore.put(id, token);
                    break;
                case REFRESH_TOKEN:
                    refreshTokenStore.put(id, token);
                    break;
                default:
                    throw new AbstractException(ExceptionCodes.INCORRECT_TOKEN_TYPE.name());
            }
        } catch (IllegalArgumentException exception) {
            throw new AbstractException(ExceptionCodes.INCORRECT_TOKEN_TYPE.name());
        }
    }

    public void removeToken(String id, String type) throws AbstractException {
        try {
            Token tokenType = Token.valueOf(type);

            switch (tokenType) {
                case ACCESS_TOKEN:
                    accessTokenStore.remove(id);
                    break;
                case REFRESH_TOKEN:
                    refreshTokenStore.remove(id);
                    break;
                default:
                    throw new AbstractException(ExceptionCodes.INCORRECT_TOKEN_TYPE.name());
            }
        } catch (IllegalArgumentException exception) {
            throw new AbstractException(ExceptionCodes.INCORRECT_TOKEN_TYPE.name());
        }
    }

    public String getTokenById(String id, String type) throws AbstractException {
        try {
            Token tokenType = Token.valueOf(type);

            switch (tokenType) {
                case ACCESS_TOKEN:
                    return accessTokenStore.get(id);
                case REFRESH_TOKEN:
                    return refreshTokenStore.get(id);
                default:
                    throw new AbstractException(ExceptionCodes.INCORRECT_TOKEN_TYPE.name());
            }
        } catch (IllegalArgumentException exception) {
            throw new AbstractException(ExceptionCodes.INCORRECT_TOKEN_TYPE.name());
        }
    }
}
