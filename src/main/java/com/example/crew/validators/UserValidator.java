package com.example.crew.validators;

import com.example.crew.dtos.UserDTO;
import com.example.crew.entities.User;
import com.example.crew.exceptions.AbstractException;
import com.example.crew.utils.enums.ExceptionCodes;
import com.example.crew.utils.enums.Role;

import java.util.regex.Pattern;

public class UserValidator {
    private static final String EMAIL_PATTERN = "^[a-zA-Z0-9_*+.-]{3,20}@[a-zA-z]{3,20}\\.[a-z]{2,3}$";
    private static final String PASSWORD_PATTERN = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-_#$^+=!*@%&]).{8,20}$";

    public static void validateEmail(String email) throws AbstractException {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);

        if (email == null)
            throw new AbstractException(ExceptionCodes.EMAIL_OF_USER_IS_NULL.getValue());
        if (!pattern.matcher(email).matches())
            throw new AbstractException(ExceptionCodes.EMAIL_OF_USER_DOES_NOT_MATCH_REQUIREMENTS.getValue());
    }

    public static void validatePassword(String password) throws AbstractException {
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);

        if (password == null)
            throw new AbstractException(ExceptionCodes.PASSWORD_OF_USER_IS_NULL.getValue());
        if (!pattern.matcher(password).matches())
            throw new AbstractException(ExceptionCodes.PASSWORD_OF_USER_DOES_NOT_MATCH_REQUIREMENTS.getValue());
    }

    public static void validateRole(String role) throws AbstractException {
        try {
            Role.valueOf(role);
        } catch (NullPointerException | IllegalArgumentException exception) {
            throw new AbstractException(ExceptionCodes.ROLE_OF_USER_IS_INVALID.getValue());
        }
    }

    public static void validate(User user, boolean withPassword) throws AbstractException {
        if (user == null)
            throw new AbstractException(ExceptionCodes.USER_IS_NULL.getValue());

        validateEmail(user.getEmail());
        validateRole(user.getRole());

        if (withPassword)
            validatePassword(user.getPassword());
    }

    public static void validate(UserDTO userDTO, boolean withPassword) throws AbstractException {
        if (userDTO == null)
            throw new AbstractException(ExceptionCodes.USER_DTO_IS_NULL.getValue());

        validateEmail(userDTO.getEmail());
        validateRole(userDTO.getRole());

        if (withPassword)
            validatePassword(userDTO.getPassword());
    }
}
