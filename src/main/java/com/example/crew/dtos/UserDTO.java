package com.example.crew.dtos;

public class UserDTO {
    private String id;
    private String role;
    private String email;
    private String password;
    private String name;

    public UserDTO() {
    }

    public UserDTO(
            String id,
            String role,
            String email,
            String password,
            String name) {
        this.id = id;
        this.role = role;
        this.email = email;
        this.password = password;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
