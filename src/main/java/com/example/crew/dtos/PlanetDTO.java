package com.example.crew.dtos;

public class PlanetDTO {
    private String id;
    private String name;
    private String image;
    private String status;
    private String description;
    private ExpeditionCrewDTO expeditionCrewDTO;

    public PlanetDTO() {
    }

    public PlanetDTO(String id, String name, String image, String status, String description, ExpeditionCrewDTO expeditionCrewDTO) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.status = status;
        this.description = description;
        this.expeditionCrewDTO = expeditionCrewDTO;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ExpeditionCrewDTO getExpeditionCrewDTO() {
        return expeditionCrewDTO;
    }

    public void setExpeditionCrewDTO(ExpeditionCrewDTO expeditionCrewDTO) {
        this.expeditionCrewDTO = expeditionCrewDTO;
    }
}
