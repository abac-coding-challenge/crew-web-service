package com.example.crew.dtos.builders;

import java.util.List;

public interface Builder<E, D> {
    D generateDTOFromEntity(E e);

    E generateEntityFromDTO(D d);

    List<D> generateDTOsFromEntities(List<E> eList);

    List<E> generateEntitiesFromDTOs(List<D> dList);
}
