package com.example.crew.dtos.builders;

import com.example.crew.dtos.CrewDTO;
import com.example.crew.entities.Crew;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CrewBuilder implements Builder<Crew, CrewDTO> {

    @Autowired
    UserBuilder userBuilder;

    @Autowired
    RobotBuilder robotBuilder;

    @Override
    public CrewDTO generateDTOFromEntity(Crew crew) {
        return new CrewDTO(
                crew.getId(),
                userBuilder.generateDTOFromEntity(crew.getUser()),
                robotBuilder.generateDTOsFromEntities(crew.getRobots()));
    }

    @Override
    public Crew generateEntityFromDTO(CrewDTO crewDTO) {
        return new Crew(
                crewDTO.getId(),
                userBuilder.generateEntityFromDTO(crewDTO.getUser()),
                robotBuilder.generateEntitiesFromDTOs(crewDTO.getRobots()),
                null);
    }

    @Override
    public List<CrewDTO> generateDTOsFromEntities(List<Crew> crews) {
        List<CrewDTO> crewDTOS = new ArrayList<>();

        for (Crew crew : crews) {
            crewDTOS.add(generateDTOFromEntity(crew));
        }

        return crewDTOS;
    }

    @Override
    public List<Crew> generateEntitiesFromDTOs(List<CrewDTO> crewDTOS) {
        List<Crew> crews = new ArrayList<>();

        for (CrewDTO crewDTO : crewDTOS) {
            crews.add(generateEntityFromDTO(crewDTO));
        }

        return crews;
    }
}
