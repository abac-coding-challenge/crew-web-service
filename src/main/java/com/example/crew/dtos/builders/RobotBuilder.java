package com.example.crew.dtos.builders;

import com.example.crew.dtos.RobotDTO;
import com.example.crew.entities.Robot;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RobotBuilder implements Builder<Robot, RobotDTO> {

    @Override
    public RobotDTO generateDTOFromEntity(Robot robot) {
        return new RobotDTO(
                robot.getId(),
                robot.getName());
    }

    @Override
    public Robot generateEntityFromDTO(RobotDTO robotDTO) {
        return new Robot(
                robotDTO.getId(),
                robotDTO.getName(),
                null);
    }

    @Override
    public List<RobotDTO> generateDTOsFromEntities(List<Robot> robots) {
        List<RobotDTO> robotDTOs = new ArrayList<>();

        for (Robot robot : robots) {
            robotDTOs.add(generateDTOFromEntity(robot));
        }

        return robotDTOs;
    }

    @Override
    public List<Robot> generateEntitiesFromDTOs(List<RobotDTO> robotDTOs) {
        List<Robot> robots = new ArrayList<>();

        for (RobotDTO robotDTO : robotDTOs) {
            robots.add(generateEntityFromDTO(robotDTO));
        }

        return robots;
    }
}
