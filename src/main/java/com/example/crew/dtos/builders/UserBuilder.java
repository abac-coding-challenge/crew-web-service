package com.example.crew.dtos.builders;

import com.example.crew.dtos.UserDTO;
import com.example.crew.entities.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserBuilder implements Builder<User, UserDTO> {

    public UserDTO generateDTOFromEntity(User user) {
        return new UserDTO(
                user.getId(),
                user.getRole(),
                user.getEmail(),
                null,
                user.getName());
    }

    public User generateEntityFromDTO(UserDTO userDTO) {
        return new User(
                userDTO.getId(),
                userDTO.getRole(),
                userDTO.getEmail(),
                userDTO.getPassword(),
                userDTO.getName(),
                null);
    }

    public List<UserDTO> generateDTOsFromEntities(List<User> users) {
        List<UserDTO> userDTOs = new ArrayList<>();

        for (User user : users) {
            userDTOs.add(generateDTOFromEntity(user));
        }

        return userDTOs;
    }

    public List<User> generateEntitiesFromDTOs(List<UserDTO> userDTOs) {
        List<User> users = new ArrayList<>();

        for (UserDTO userDTO : userDTOs) {
            users.add(generateEntityFromDTO(userDTO));
        }

        return users;
    }
}
