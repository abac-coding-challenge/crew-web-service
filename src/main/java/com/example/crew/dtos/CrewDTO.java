package com.example.crew.dtos;

import java.util.ArrayList;
import java.util.List;

public class CrewDTO {
    private String id;
    private UserDTO user;
    private List<RobotDTO> robots;

    public CrewDTO() {
        robots = new ArrayList<>();
    }

    public CrewDTO(String id, UserDTO user, List<RobotDTO> robots) {
        this.id = id;
        this.user = user;
        this.robots = robots;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public List<RobotDTO> getRobots() {
        return robots;
    }

    public void setRobots(List<RobotDTO> robots) {
        this.robots = robots;
    }
}
