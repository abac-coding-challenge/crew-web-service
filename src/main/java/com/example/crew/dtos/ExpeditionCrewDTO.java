package com.example.crew.dtos;

import java.util.ArrayList;
import java.util.List;

public class ExpeditionCrewDTO {
    private String id;
    private String captain;
    private List<String> robots;

    public ExpeditionCrewDTO() {
        robots = new ArrayList<>();
    }

    public ExpeditionCrewDTO(String id, String captain, List<String> robots) {
        this.id = id;
        this.captain = captain;
        this.robots = robots;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCaptain() {
        return captain;
    }

    public void setCaptain(String captain) {
        this.captain = captain;
    }

    public List<String> getRobots() {
        return robots;
    }

    public void setRobots(List<String> robots) {
        this.robots = robots;
    }
}
