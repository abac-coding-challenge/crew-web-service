package com.example.crew.repositories;

import com.example.crew.entities.Crew;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CrewRepository extends JpaRepository<Crew, String> {
    Optional<Crew> findByUser_Id(String id);
}
