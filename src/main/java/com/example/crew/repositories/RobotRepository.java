package com.example.crew.repositories;

import com.example.crew.entities.Robot;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RobotRepository extends JpaRepository<Robot, String> {
}
