package com.example.crew.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "crews")
public class Crew {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(updatable = false, nullable = false)
    private String id;

    @JsonManagedReference
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_CREW_USER"))
    private User user;

    @JsonBackReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "crew", cascade = CascadeType.REMOVE)
    private List<Robot> robots;

    @JsonBackReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "crew")
    private List<Planet> planets;

    public Crew() {
        robots = new ArrayList<>();
    }

    public Crew(String id, User user, List<Robot> robots, List<Planet> planets) {
        this.id = id;
        this.user = user;
        this.robots = robots;
        this.planets = planets;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Robot> getRobots() {
        return robots;
    }

    public void setRobots(List<Robot> robots) {
        this.robots = robots;
    }

    public List<Planet> getPlanets() {
        return planets;
    }

    public void setPlanets(List<Planet> planets) {
        this.planets = planets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Crew)) return false;

        Crew crew = (Crew) o;

        return crew.getId().equals(id);
    }
}
