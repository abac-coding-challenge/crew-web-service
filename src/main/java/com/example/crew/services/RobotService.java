package com.example.crew.services;

import com.example.crew.dtos.RobotDTO;
import com.example.crew.dtos.builders.RobotBuilder;
import com.example.crew.entities.Crew;
import com.example.crew.entities.Robot;
import com.example.crew.exceptions.AbstractException;
import com.example.crew.exceptions.NotFoundException;
import com.example.crew.repositories.RobotRepository;
import com.example.crew.utils.enums.ExceptionCodes;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RobotService {
    private final RobotRepository robotRepository;
    private final RobotBuilder robotBuilder;
    private final CrewService crewService;

    public RobotService(RobotRepository robotRepository, RobotBuilder robotBuilder, CrewService crewService) {
        this.robotRepository = robotRepository;
        this.robotBuilder = robotBuilder;
        this.crewService = crewService;
    }

    public List<Robot> getAllByUserId(String userId) {
        List<Robot> robots = robotRepository.findAll();

        return robots.stream().filter(r -> r.getCrew().getUser().getId().equals(userId)).collect(Collectors.toList());
    }

    public Robot add(String userId, RobotDTO robotDTO) throws NotFoundException {
        Crew crew = crewService.getByUserId(userId);
        Robot robot = robotBuilder.generateEntityFromDTO(robotDTO);
        robot.setCrew(crew);

        return robotRepository.save(robot);
    }

    public Robot edit(String id, RobotDTO robotDTO) throws AbstractException {
        Optional<Robot> optionalRobot = robotRepository.findById(id);

        if (optionalRobot.isEmpty()) {
            throw new NotFoundException(ExceptionCodes.ROBOT_NOT_FOUND_BY_ID.getValue());
        }

        Robot robot = optionalRobot.get();

        if (robotDTO.getName() != null) {
            robot.setName(robotDTO.getName());
        }

        robotRepository.save(robot);

        return robot;
    }

    public void remove(String id) throws NotFoundException {
        Optional<Robot> optionalRobot = robotRepository.findById(id);

        if (optionalRobot.isEmpty()) {
            throw new NotFoundException(ExceptionCodes.ROBOT_NOT_FOUND_BY_ID.getValue());
        }

        robotRepository.delete(optionalRobot.get());
    }
}
