package com.example.crew.services;

import com.example.crew.dtos.AuthorizationTokenDTO;
import com.example.crew.dtos.UserDTO;
import com.example.crew.entities.User;
import com.example.crew.exceptions.AbstractException;
import com.example.crew.models.TokenStorage;
import com.example.crew.utils.JwtTokenUtil;
import com.example.crew.utils.enums.ExceptionCodes;
import com.example.crew.utils.enums.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthorizationTokenService {
    private final JwtTokenUtil jwtTokenUtil;
    private final UserService userService;
    private final TokenStorage tokenStorage;

    @Autowired
    public AuthorizationTokenService(JwtTokenUtil jwtTokenUtil, UserService userService) {
        super();
        this.jwtTokenUtil = jwtTokenUtil;
        this.tokenStorage = TokenStorage.getInstance();
        this.userService = userService;
    }

    @Autowired
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public AuthorizationTokenDTO createToken(UserDTO userDTO) throws AbstractException {
        User user = userService.getByEmail(userDTO.getEmail(), true);

        String encodedPassword = user.getPassword();
        String rawPassword = userDTO.getPassword();

        if (!passwordEncoder().matches(rawPassword, encodedPassword)) {
            throw new AbstractException(ExceptionCodes.BAD_CREDENTIALS.getValue());
        }

        return createTokenResponse(user);
    }

    public AuthorizationTokenDTO refreshToken(String refreshToken) throws AbstractException {
        String id = jwtTokenUtil.getIdFromToken(refreshToken);
        User user = userService.getById(id);

        String actualRefreshToken = tokenStorage.getTokenById(id, Token.REFRESH_TOKEN.getValue());

        if (actualRefreshToken == null || !actualRefreshToken.equals(refreshToken)) {
            throw new AbstractException(ExceptionCodes.INCORRECT_REFRESH_TOKEN.getValue());
        }

        return createTokenResponse(user);
    }

    public void removeToken(String id) throws AbstractException {
        tokenStorage.removeToken(id, Token.ACCESS_TOKEN.getValue());
        tokenStorage.removeToken(id, Token.REFRESH_TOKEN.getValue());
    }

    private AuthorizationTokenDTO createTokenResponse(User user) throws AbstractException {
        String accessToken = jwtTokenUtil.generateToken(user, Token.ACCESS_TOKEN.getValue());
        String refreshToken = jwtTokenUtil.generateToken(user, Token.REFRESH_TOKEN.getValue());

        tokenStorage.addToken(user.getId(), accessToken, Token.ACCESS_TOKEN.getValue());
        tokenStorage.addToken(user.getId(), refreshToken, Token.REFRESH_TOKEN.getValue());

        return new AuthorizationTokenDTO(accessToken, refreshToken, user.getId());
    }
}
