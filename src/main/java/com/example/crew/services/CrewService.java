package com.example.crew.services;

import com.example.crew.entities.Crew;
import com.example.crew.entities.User;
import com.example.crew.exceptions.NotFoundException;
import com.example.crew.repositories.CrewRepository;
import com.example.crew.utils.enums.ExceptionCodes;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CrewService {
    private final CrewRepository crewRepository;

    public CrewService(CrewRepository crewRepository) {
        this.crewRepository = crewRepository;
    }

    public Crew getByUserId(String id) throws NotFoundException {
        Optional<Crew> optionalCrew = crewRepository.findByUser_Id(id);

        if (optionalCrew.isEmpty()) {
            throw new NotFoundException(ExceptionCodes.CREW_NOT_FOUND_BY_ID.getValue());
        }

        return optionalCrew.get();
    }

    public List<Crew> getAll() {
        return crewRepository.findAll();
    }

    public Crew create(User user) {
        Crew crew = new Crew();
        crew.setUser(user);

        crewRepository.save(crew);

        return crew;
    }
}
