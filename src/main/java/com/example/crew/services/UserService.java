package com.example.crew.services;

import com.example.crew.dtos.UserDTO;
import com.example.crew.dtos.builders.UserBuilder;
import com.example.crew.entities.Crew;
import com.example.crew.entities.User;
import com.example.crew.exceptions.AbstractException;
import com.example.crew.exceptions.NotFoundException;
import com.example.crew.repositories.UserRepository;
import com.example.crew.utils.enums.ExceptionCodes;
import com.example.crew.utils.enums.Role;
import com.example.crew.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final CrewService crewService;
    private final UserBuilder userBuilder;

    @Autowired
    public UserService(
            UserRepository userRepository,
            CrewService crewService, UserBuilder userBuilder) {
        this.userRepository = userRepository;
        this.crewService = crewService;
        this.userBuilder = userBuilder;
    }

    @Autowired
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public User getByEmail(String email, boolean withPassword) throws NotFoundException {
        Optional<User> optionalUser = userRepository.findByEmail(email);

        if (optionalUser.isEmpty()) {
            throw new NotFoundException(ExceptionCodes.USER_NOT_FOUND_BY_EMAIL.getValue());
        }

        User user = optionalUser.get();

        if (!withPassword) {
            user.setPassword(null);
        }

        return user;
    }

    public User getById(String id) throws NotFoundException {
        Optional<User> optionalUser = userRepository.findById(id);

        if (optionalUser.isEmpty()) {
            throw new NotFoundException(ExceptionCodes.USER_NOT_FOUND_BY_ID.getValue());
        }

        return optionalUser.get();
    }

    public User create(UserDTO userDTO) throws AbstractException {
        userDTO.setRole(Role.ROLE_CAPTAIN.getValue());
        UserValidator.validate(userDTO, true);

        Optional<User> optionalUserFromEmail = userRepository.findByEmail(userDTO.getEmail());

        if (optionalUserFromEmail.isPresent()) {
            throw new AbstractException(ExceptionCodes.ALREADY_EXISTING_EMAIL.getValue());
        }

        User user = userBuilder.generateEntityFromDTO(userDTO);
        user.setPassword(passwordEncoder().encode(user.getPassword()));

        userRepository.save(user);
        Crew crew = crewService.create(user);
        user.setCrew(crew);

        return user;
    }
}
