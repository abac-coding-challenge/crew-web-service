# Crew Web Service

## Run the applications

### XPAND Android application

For use the XPAND client application you must download the `app-release.apk` and install on your emulated smartphone.
* Minimum SDK is **Android 7.0** (API level 24)
* Recommended SDK is **Android 10** (API level 29)

### Crew web service

For running the Crew web service you need to download the [crew-1.0.0.jar](crew-1.0.0.jar) and run with following command `java -jar crew-1.0.0.jar`.  
It is necessary to have **Java SE Development Kit 11** installed on your hardware and requires JAVA_HOME to be set as environment variable to work correctly. JAVA_HOME should point to a JDK directory not a JRE one.

### Planet web service

For running the Crew web service you need to download the `planet-1.0.0.jar` and run with following command `java -jar planet-1.0.0.jar`.  
It is necessary to have **Java SE Development Kit 11** installed on your hardware and requires JAVA_HOME to be set as environment variable to work correctly. JAVA_HOME should point to a JDK directory not a JRE one.

### Database

System used MySQL database, it can be downloaded: [MySQL Workbench](https://dev.mysql.com/downloads/workbench/).  
After a successfully installation you need to create a new user within the MySQL shell: `CREATE USER 'user'@'localhost' IDENTIFIED BY 'secret';` and provide the user with access to the information they will need: `GRANT ALL PRIVILEGES ON * . * TO 'user'@'localhost';`.  
In MySQL Workbench you can create a new connection with the following parameters:
* Connection Name: `<write a name>`
* Connection Method: `Standard (TCP/IP)`
* Hostname: `127.0.0.1`
* Port: `3306`
* Username: `user`
* Password: `secret`  

After when connection is made successfully you need to create a schema by right click on `SCHEMAS` and selected `Create Schema...`. Name must be `expedition`.

## Features

* User can register/**sign up** to application with a name, an email and a password.
* User can and have to **sign in** in the application to use services with email and password.
* User can **sign out** from the application.
* User can **add a new planet** with values: name, image, status and description.
* User can **list all of saved planets**.
* User can **select a planet** from listed ones and can see the values: name, image, status and description, associated crew.
* User can **modify planet values** and **delete the planet** from the system.
* User can **add a new robot** in his crew with value: name.
* User can **list all of his saved robots**.
* User can **select a robot** from listed ones and can see the value: name.
* User can **modify robot values** and **delete the robot** from the system.
* When a user creates a new account (signs up to application) is **created automatically a crew** (without robots) associated to his account.
* **Single Sign On**: After a successfully log in the crew web service creates two Json Web Tokens, an access token for authorization (validity 1 hour), and a refresh token (validity 24 hours) for getting a new valid access token.
* If user does not use the application (does not make any modifications) he is **redirected automatically** to `Sign in` page.
* User gets **specific error messages** if introduces invalid data.

## Use case diagram

![alt text](doc/use-case.png)

## Architecture diagram

![alt text](doc/architecture.png)
